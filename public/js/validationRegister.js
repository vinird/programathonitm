$( document ).ready(function() {
  $("#botonEmpresaSig").click(function() {
    if(validarEmpresa()) {
      $("#tabEmpresa").parent().removeClass("active");
      $("#tabRedes").attr("data-toggle", "tab").parent().removeClass("disabled").addClass("active");
      $("#empresa").removeClass("in active");
      $("#redes").addClass("in active");
    }
  });

  $("#botonEmpresaAnt").click(function() {
    $("#tabRedes").parent().removeClass("active");
    $("#tabEmpresa").parent().addClass("active");
    $("#redes").removeClass("in active");
    $("#empresa").addClass("in active");
  });

  $("#botonRedesSig").click(function() {
    if(validarRedes()) {
      $("#tabRedes").parent().removeClass("active");
      $("#tabUsuario").attr("data-toggle", "tab").parent().removeClass("disabled").addClass("active");
      $("#redes").removeClass("in active");
      $("#usuario").addClass("in active");
    }
  });

  $("#botonEnviarRegistro").click(function() {
    if(validarUsuario()) {
      $('#formRegistro').submit();
    }
  });

  $('.iCheck-helper').click(function () {
    if(!$("#pymeActiva").parent().hasClass('checked') && $(this).prev()[0].id == "pymeActiva"){
       $("#modalConfirmar").modal({backdrop: 'static', keyboard: false});
    }
  });

  $("#botonCancelar, #botonCerrar").click(function() {
    $("#pymeActiva").parent().addClass("checked");
    $("#pymeActiva").prop("checked", true);
  });

  $("#botonSi").click(function() {
    $("#pymeActiva").parent().removeClass("checked");
    $("#pymeActiva").prop("checked", false);
  });

  function validarEmpresa() {
    if(!$.trim($('#nombreComercial').val()).length > 0) {
      $('#nombreComercial').css("border", "1px solid red");
      mensajeError(true, "El campo Nombre comercial es obligatorio.");
      return false;
    } else {
      mensajeError(false, "");
      $('#nombreComercial').css("border", "1px solid #d2d6de");
    }
    if($("#pais option:selected").index() === 0) {
      $('#pais').css("border", "1px solid red");
      mensajeError(true, "El campo País es obligatorio.");
      return false;
    } else {
      mensajeError(false, "");
      $('#pais').css("border", "1px solid #d2d6de");
    }
    if($("#provincia option:selected").index() === 0) {
      $('#provincia').css("border", "1px solid red");
      mensajeError(true, "El campo Provincia/Estado es obligatorio.");
      return false;
    } else {
      mensajeError(false, "");
      $('#provincia').css("border", "1px solid #d2d6de");
    }
    if($("#sector option:selected").index() === 0) {
      $('#sector').css("border", "1px solid red");
      mensajeError(true, "El campo Sector es obligatorio.");
      return false;
    } else {
      mensajeError(false, "");
      $('#sector').css("border", "1px solid #d2d6de");
    }
    if($("#annoEmpresa option:selected").index() === 0) {
      $('#annoEmpresa').css("border", "1px solid red");
      mensajeError(true, "El campo Año de inicio de operaciones es obligatorio.");
      return false;
    } else {
      mensajeError(false, "");
      $('#annoEmpresa').css("border", "1px solid #d2d6de");
    }
    if($("#generoPropietario option:selected").index() === 0) {
      $('#generoPropietario').css("border", "1px solid red");
      mensajeError(true, "El campo Género del propietario es obligatorio.");
      return false;
    } else {
      mensajeError(false, "");
      $('#generoPropietario').css("border", "1px solid #d2d6de");
    }
    if(!$.trim($('#cedulaEmpresa').val()).length > 0) {
      $('#cedulaEmpresa').css("border", "1px solid red");
      mensajeError(true, "El campo Cédula jurídica es obligatorio.");
      return false;
    } else {
      mensajeError(false, "");
      $('#cedulaEmpresa').css("border", "1px solid #d2d6de");
    }
    if(!$.trim($('#telefonoEmpresa').val()).length > 0) {
      $('#telefonoEmpresa').css("border", "1px solid red");
      mensajeError(true, "El campo Teléfono de la empresa es obligatorio.");
      return false;
    } else {
      mensajeError(false, "");
      $('#telefonoEmpresa').css("border", "1px solid #d2d6de");
    }
    if(!$.trim($('#direccionEmpresa').val()).length > 0) {
      $('#direccionEmpresa').css("border", "1px solid red");
      mensajeError(true, "El campo Dirección de la empresa es obligatorio.");
      return false;
    } else {
      mensajeError(false, "");
      $('#direccionEmpresa').css("border", "1px solid #d2d6de");
    }
    return true;
  }

  function validarRedes() {
    if(!$.trim($('#facebookEmpresa').val()).length > 0) {
      $('#facebookEmpresa').css("border", "1px solid red");
      mensajeError(true, "El campo página de Facebook es obligatorio.");
      return false;
    } else {
      mensajeError(false, "");
      $('#facebookEmpresa').css("border", "1px solid #d2d6de");
    }
    if($("#correoEmpresa").val().length > 0 && !verificarCorreo($("#correoEmpresa").val())) {
      $('#correoEmpresa').css("border", "1px solid red");
      mensajeError(true, "El campo Correo electrónico de contacto no corresponde con una dirección de e-mail válida.");
      return false;
    } else {
      mensajeError(false, "");
      $('#correoEmpresa').css("border", "1px solid #d2d6de");
    }
    return true;
  }

  function validarUsuario() {
    if(!$.trim($('#name').val()).length > 0) {
      $('#name').css("border", "1px solid red");
      mensajeError(true, "El campo Nombre completo es obligatorio.");
      return false;
    } else {
      mensajeError(false, "");
      $('#name').css("border", "1px solid #d2d6de");
    }
    if(!$.trim($('#username').val()).length > 0) {
      $('#username').css("border", "1px solid red");
      mensajeError(true, "El campo Nombre de usuario es obligatorio.");
      return false;
    } else {
      mensajeError(false, "");
      $('#username').css("border", "1px solid #d2d6de");
    }
    if(!$.trim($('#password').val()).length > 0) {
      $('#password').css("border", "1px solid red");
      mensajeError(true, "El campo Contraseña es obligatorio.");
      return false;
    } else {
      if($('#password').val().length < 8 || $('#password').val().length > 10) {
        $('#password').css("border", "1px solid red");
        mensajeError(true, "El campo Contraseña debe contener entre 8 y 10 caracteres.");
        return false;
      } else {
        mensajeError(false, "");
        $('#password').css("border", "1px solid #d2d6de");
      }
    }
    if(!$.trim($('#password_confirmation').val()).length > 0) {
      $('#password_confirmation').css("border", "1px solid red");
      mensajeError(true, "Debe Volver a escribir la contraseña.");
      return false;
    } else {
      if($('#password').val() !== $('#password_confirmation').val()) {
        $('#password').css("border", "1px solid orange");
        $('#password_confirmation').css("border", "1px solid orange");
        mensajeError(true, "Los campos Contraseña y Volver a escribir la contraseña deben coincidir.");
        return false;
      } else {
        mensajeError(false, "");
        $('#password').css("border", "1px solid #d2d6de");
        $('#password_confirmation').css("border", "1px solid #d2d6de");
      }
    }
    if(!$("#email").val().length && !verificarCorreo($("#email").val())) {
      $('#email').css("border", "1px solid red");
      mensajeError(true, "El campo Correo es obligatorio.");
      return false;
    } else {
      mensajeError(false, "");
      $('#email').css("border", "1px solid #d2d6de");
    }
    if(!$.trim($('#email_confirmation').val()).length > 0) {
      $('#email_confirmation').css("border", "1px solid red");
      $("#messageErrors").removeClass("hidden");
      $("#listErrors").prepend("<li>Debe Volver a escribir el correo.</li>");
      return false;
    } else {
      if($('#email').val() !== $('#email_confirmation').val()) {
        $('#email').css("border", "1px solid orange");
        $('#email_confirmation').css("border", "1px solid orange");
        mensajeError(true, "Los campos Correo y Volver a escribir el correo deben coincidir.");
        return false;
      } else {
        mensajeError(false, "");
        $('#email').css("border", "1px solid #d2d6de");
        $('#email_confirmation').css("border", "1px solid #d2d6de");
      }
    }
    return true;
  }

  function mensajeError(esVisibe, mensaje) {
    if (esVisibe) {
      $("#messageErrors").removeClass("hidden");
      $("#listErrors").prepend("<li>"+ mensaje +"</li>");
    } else {
      $("#messageErrors").addClass("hidden");
      $("#listErrors").empty();
    }
  }

  function verificarCorreo(email) {
    return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(email);
  }
});

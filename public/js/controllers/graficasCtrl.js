jQuery(document).ready(function($) {
	var render = true;
	$('input[name="daterange"]').daterangepicker({
		locale: {
			cancelLabel: 'Cancelar',
			applyLabel: 'Consultar',
			fromLabel: 'De:',
			toLabel: 'Hasta'
		},
		maxDate: moment().endOf("day"),
	}, function(start, end, label) {
		startDate = start;
		endDate = end;
		renderCharts();
	});

	var startDate = $('input[name="daterange"]').data('daterangepicker').startDate;
	var endDate = $('input[name="daterange"]').data('daterangepicker').endDate;

	$('input[name="daterange"]').val( startDate.format("MM/DD/YYYY") +" - "+  endDate.format("MM/DD/YYYY"));

	$('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
		//do something, like clearing an input
		startDate = picker.startDate;
		endDate = picker.endDate;
		renderCharts();
	});
	function renderCharts() {
		var	respuesta01 = 0;
		var respuesta02 = 0;
		var respuesta03 = 0;
		var respuesta04 = 0;
		var respuesta05 = 0;


		var resultados = {
			genero:{
				M:0,
				F:0,
				N:0
			},
			rangoEdad:{
				r1: 0,
				r2: 0,
				r3: 0,
				r4: 0,
				r5: 0,
				r6: 0,
				r7: 0
			}
		};


		var count = 0;
		respuestas.forEach(function(respuesta){
			// comprobar si la fecha esta entre otras dos
			//moment(respuestas[0].FechaRespuesta)
			if( moment(respuesta.FechaRespuesta).isBetween(startDate, endDate) ){
				count++;
				respuesta01 += respuesta.Respuesta01;
				respuesta02 += respuesta.Respuesta02;
				respuesta03 += respuesta.Respuesta03;
				respuesta04 += respuesta.Respuesta04;
				respuesta05 += respuesta.Respuesta05;

				switch(respuesta.GeneroID){
					case "M":
						resultados.genero.M ++;
						break;
					case "F":
						resultados.genero.F ++;
						break;
					default: resultados.genero.N ++;
				}

				switch(respuesta.RangoEdad){
					case 1:
						resultados.rangoEdad.r1 ++;
						break;
					case 2:
						resultados.rangoEdad.r2 ++;
						break;
					case 3:
						resultados.rangoEdad.r3 ++;
						break;
					case 4:
						resultados.rangoEdad.r4 ++;
						break;
					case 5:
						resultados.rangoEdad.r5 ++;
						break;
					case 6:
						resultados.rangoEdad.r6 ++;
						break;
					case 7:
						resultados.rangoEdad.r7 ++;
						break;
				}
			}
		});
		//
		respuesta01 = respuesta01 / count;
		respuesta02 = respuesta02 / count;
		respuesta03 = respuesta03 / count;
		respuesta04 = respuesta04 / count;
		respuesta05 = respuesta05 / count;

		var ctx = document.getElementById("chartRespuestas");
		var myChart = new Chart(ctx, {

			type: 'bar',
			data: {
				labels: [
					"Calidad del producto o servicio",
					"Tiempo de espera en la atención",
					"Imagen de las instalaciones",
					"Disponibilidad de producto o servicio solicitado ",
					"Atención del personal"],
				datasets: [{
					label: "Total de respuestas: " + count,
					data: [respuesta01, respuesta02, respuesta03, respuesta04, respuesta05],
					backgroundColor: [
						'rgba(255, 99, 132, 0.8)',
						'rgba(54, 162, 235, 0.8)',
						'rgba(255, 206, 86, 0.8)',
						'rgba(75, 192, 192, 0.8)',
						'rgba(153, 102, 255, 0.8)'
					],
					borderColor: [
						'rgba(255,99,132,1)',
						'rgba(54, 162, 235, 1)',
						'rgba(255, 206, 86, 1)',
						'rgba(75, 192, 192, 1)',
						'rgba(153, 102, 255, 1)'
					],
					borderWidth: 1
				}]
			},
			options: {
				scales: {
					yAxes: [{
						display: true,
						ticks: {
							beginAtZero: true,
							max: 5,
							stepSize: 1
						}
					}],
					xAxes: [{
						display: false
					}]
				}
			},
			legend:{
				display: false
			}
		});

		var chartGenero = document.getElementById("chartGenero");
		var myChart2 = new Chart(chartGenero, {
			type: 'doughnut',
			data: {
				labels: ["Masculino", "Femenino", "No aplica"],
				datasets: [{
					label: '# of Votes',
					data: [resultados.genero.M, resultados.genero.F, resultados.genero.N],
					backgroundColor: [
						'rgba(255, 99, 132, 0.8)',
						'rgba(54, 162, 235, 0.8)',
						'rgba(255, 206, 86, 0.8)'
					],
					borderColor: [
						'rgba(255,99,132,1)',
						'rgba(54, 162, 235, 1)',
						'rgba(255, 206, 86, 1)'
					],
					borderWidth: 1
				}]
			},
			options: {
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero:true
						}
					}]
				}
			},
			responsive: true,
			maintainAspectRatio: false
		});

		var chartEdad = document.getElementById("chartEdad");
		var myChart3 = new Chart(chartEdad, {
			type: 'doughnut',
			data: {
				labels: ["12-17", "18-33", "34-45", "46-55", "56-64", "65-73", "74+"],
				datasets: [{
					label: '# of Votes',
					data: [resultados.rangoEdad.r1, resultados.rangoEdad.r2, resultados.rangoEdad.r3, resultados.rangoEdad.r4,
						resultados.rangoEdad.r5, resultados.rangoEdad.r6, resultados.rangoEdad.r7],
					backgroundColor: [
						'rgba(255, 99, 132, 0.8)',
						'rgba(54, 162, 235, 0.8)',
						'rgba(154, 222, 235, 0.8)',
						'rgba(255, 126, 86, 0.8)',
						'rgba(75, 192, 192, 0.8)',
						'rgba(153, 102, 255, 0.8)',
						'rgba(205, 176, 86, 0.8)'
					],
					borderColor: [
						'rgba(255, 99, 132, 1)',
						'rgba(54, 162, 235, 1)',
						'rgba(154, 222, 235, 1)',
						'rgba(255, 126, 86, 1)',
						'rgba(75, 192, 192, 1)',
						'rgba(153, 102, 255, 1)',
						'rgba(205, 176, 86, 1)'
					],
					borderWidth: 1
				}]
			},
			options: {
				scales: {
					yAxes: [{
						ticks: {
							beginAtZero:true
						}
					}]
				}
			},
			responsive: true,
			maintainAspectRatio: false
		});
	}

	renderCharts();
});
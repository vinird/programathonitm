@extends('layouts.auth')

@section('htmlheader_title')
Register
@endsection

@section('content')

<body class="hold-transition register-page">
  <div class="register-box">
    <div class="register-logo">
      <a href="{{ url('/home') }}"><b>La Voz</b> del Cliente</a>
    </div>

    @if (count($errors) > 0)
    <div class="alert alert-danger">
      <strong>Whoops!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
      <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
    @endif

    <div class="register-box-body">
      <div class="form-box">
        <!-- Formulario de Modificación de datos de Pymes -->
        <form id="formRegistro" action="{{ route('pyme.update', $pyme->Id) }}" enctype="multipart/form-data" method="POST">
          <!-- Token para formularios de Laravel -->
          {{ csrf_field() }}
          <!--  -->
          <!-- Método de envío -->
          <input type="hidden" name="_method" value="PUT">
          <!--  -->
          <p class="login-box-msg">{{ trans('messages.modifyEmpresa') }}</p><!-- Titulo  -->

          <!-- NombreComercio [No modificable] -->
          <div class="form-group has-feedback">
            <label for="nombreComercial">{{ trans('messages.nombreComercial') }}</label>
            <input id="nombreComercial" type="text" class="form-control"  name="nombreComercial" value="{{ $pyme->NombreComercio }}" maxlength="100" disabled/>
            <span class="glyphicon glyphicon-briefcase form-control-feedback"></span>
          </div>

          <!-- Pais  -->
          <div class="form-group has-feedback">
            <label for="paisEmpresa">{{ trans('messages.paisEmpresa') }}</label>
            <select class="form-control" name="pais" required>
              <option value=""></option>
              @if(isset($paises))
              @if(count($paises) > 0)
              @foreach($paises as $pais)
              <option value="{{ $pais->Id }}" @if($miPais->Id==$pais->Id) selected @endif>{{ $pais->Nombre}}</option>
              @endforeach
              @endif
              @endif
            </select>
            <span class="glyphicon glyphicon-globe form-control-feedback select-glyphicon"></span>
          </div>

          <!-- Estado -->
          <div class="form-group has-feedback">
            <label for="provinciaEmpresa">{{ trans('messages.provinciaEmpresa') }}</label>
            <select class="form-control" name="provincia" required>
              <option value=""></option>
              @if(isset($estados))
              @if(count($estados) > 0)
              @foreach($estados as $estado)
              <option value="{{ $estado->Id }}" @if($estado->Id==$pyme->EstadoID) selected @endif>{{ $estado->Nombre}}</option>
              @endforeach
              @endif
              @endif
            </select>
            <span class="glyphicon glyphicon-globe form-control-feedback select-glyphicon"></span>
          </div>

          <!-- Sector -->
          <div class="form-group has-feedback">
            <label for="sectorEmpresa">{{ trans('messages.sectorEmpresa') }}</label>
            <select class="form-control" name="sector" required>
              <option value=""></option>
              @if(isset($sectores))
              @if(count($sectores) > 0)
              @foreach($sectores as $sector)
              <option value="{{ $sector->Id }}" @if($sector->Id==$pyme->SectorID) selected @endif>{{ $sector->Nombre}}</option>
              @endforeach
              @endif
              @endif
            </select>
            <span class="glyphicon glyphicon-briefcase form-control-feedback select-glyphicon"></span>
          </div>

          <!-- AnnoInicioOperaciones -->
          <div class="form-group has-feedback">
            <label for="annoEmpresa">{{ trans('messages.annoEmpresa') }}</label>
            <select class="form-control" name="annoEmpresa" required>
              <option value=""></option>
              @if(isset($annos))
              @if(count($annos) > 0)
              @foreach($annos as $anno)
              <option value="{{ $anno }}"  @if(intval(substr(strval($anno),2)) == $pyme->AnnoInicioOperaciones) selected @endif  >{{ $anno }}</option>
              @endforeach
              @endif
              @endif
            </select>
            <span class="glyphicon glyphicon-calendar form-control-feedback select-glyphicon"></span>
          </div>

          <!-- Genero -->
          <div class="form-group has-feedback">
            <label for="generoPropietario">{{ trans('messages.generoPropietario') }}</label>
            <select class="form-control" name="generoPropietario" required>
              <option value=""></option>
              @if(isset($generos))
              @if(count($generos) > 0)
              @foreach($generos as $genero)
              <option value="{{ $genero->Id }}"  @if($genero->Id==$pyme->GeneroPropietarioID) selected @endif >{{ $genero->Nombre}}</option>
              @endforeach
              @endif
              @endif
            </select>
            <span class="glyphicon glyphicon-user form-control-feedback select-glyphicon"></span>
          </div>

          <!-- CedJuridica -->
          <div class="form-group has-feedback">
            <input id="cedulaEmpresa" type="text" class="form-control" placeholder="{{ trans('messages.cedulaEmpresa') }}" name="cedulaEmpresa" value="{{ $pyme->CedJuridica }}" required maxlength="50"/>
            <span class="glyphicon glyphicon-briefcase form-control-feedback"></span>
          </div>

          <!-- NumeroTelefono -->
          <div class="form-group has-feedback">
            <input id="telefonoEmpresa" type="text" class="form-control" placeholder="{{ trans('messages.telefonoEmpresa') }}" name="telefonoEmpresa" value="{{ $pyme->NumeroTelefono }}" required maxlength="50"/>
            <span class="glyphicon glyphicon-phone form-control-feedback"></span>
          </div>

          <!-- Direccion -->
          <div class="form-group has-feedback">
            <textarea id="direccionEmpresa" class="form-control" placeholder="{{ trans('messages.direccionEmpresa') }}" name="direccionEmpresa" maxlength="200" required>{{ $pyme->Direccion }}</textarea>
            <span class="glyphicon glyphicon-map-marker form-control-feedback"></span>
          </div>

          <!-- EsActiva -->
          <div class="form-group has-feedback">
            <input id="pymeActiva" type="checkbox" name="pymeActiva" @if($pyme->EsActiva) checked @endif/>
            <label for="pymeActiva">{{ trans('messages.pymeActiva') }}</label>
          </div>

          <!-- EsNegocioFamiliar -->
          <div class="form-group has-feedback">
            <input id="negocioFamiliar" type="checkbox" class="form-control" name="negocioFamiliar" @if($pyme->EsNegocioFamiliar) checked @endif/>
            <label for="negocioFamiliar">{{ trans('messages.negocioFamiliar') }}</label>
          </div>

          <!-- Logo (ExtensionLogo) -->
          <div class="form-group has-feedback">
            <label for="logoEmpresa">{{ trans('messages.logoEmpresa') }}</label>
            <input id="logoEmpresa" type="file" class="form-control" name="logoEmpresa"/>
          </div>

          <div class="col-xs-12">
            <button type="submit" class="btn btn-warning btn-block btn-flat">{{ trans('messages.modificarBoton') }}</button>
          </div>

        </form><!-- /.formulario de registro de usuarios -->
        <br><br><br>
        <a href="{{ url('/home') }}" class="text-center">{{ trans('messages.cancelarBoton') }}</a>
      </div><!-- /.form-box -->
    </div><!-- /.register-box-body -->
  </div><!-- /.register-box -->

  @include('layouts.partials.scripts_auth')

  @include('auth.terms')

  <script>
  $(function () {
    $("input").parent().click();
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
  </script>
</body>

<div id="modalConfirmar" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button id="botonCerrar" type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <p>¿Desea desactivar la PYME?</p>
      </div>
      <div class="modal-footer">
        <button id="botonCancelar" type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button id="botonNo" type="button" data-dismiss="modal" class="btn btn-danger">Deshabilitar</button>
      </div>
    </div>
  </div>
</div>

@endsection

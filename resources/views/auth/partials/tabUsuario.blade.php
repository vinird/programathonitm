<p class="login-box-msg">{{ trans('messages.registermember') }}</p>

<!-- NombreCompleto -->
<div class="form-group has-feedback">
  <input id="name" type="text" class="form-control" placeholder="{{ trans('messages.fullname') }}" name="name" value="{{ old('name') }}" required maxlength="50"/>
  <span class="glyphicon glyphicon-user form-control-feedback"></span>
</div>

<!-- Usuario -->
<div class="form-group has-feedback">
  <input id="username" type="text" class="form-control" placeholder="{{ trans('messages.nombreUsuario') }}" name="username" value="{{ old('username') }}" required maxlength="50"/>
  <span class="glyphicon glyphicon-user form-control-feedback"></span>
</div>

<!-- Clave -->
<div class="form-group has-feedback">
  <input id="password" type="password" class="form-control" placeholder="{{ trans('messages.password') }}" name="password" minlength="8" maxlength="10" required/>
  <span class="glyphicon glyphicon-lock form-control-feedback" ></span>
</div>

<!-- Confirmar Clave -->
<div class="form-group has-feedback">
  <input id="password_confirmation" type="password" class="form-control" placeholder="{{ trans('messages.retrypassword') }}" name="password_confirmation" required minlength="8" maxlength="10"/>
  <span class="glyphicon glyphicon-lock form-control-feedback"></span>
</div>

<!-- EmailContacto -->
<div class="form-group has-feedback">
  <input id="email" type="email" class="form-control" placeholder="{{ trans('messages.email') }}" name="email" value="{{ old('email') }}" required maxlength="50"/>
  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
</div>

<!-- Confirmar EmailContacto -->
<div class="form-group has-feedback">
  <input id="email_confirmation" type="email" class="form-control" placeholder="{{ trans('messages.retryEmail') }}" name="email_confirmation" value="{{ old('email_confirmation') }}" required maxlength="50"/>
  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
</div>

<!-- Cancelar Btn -->
<div class="col-xs-6">
  <button id="botonCancelar" type="button" class="btn btn-default btn-block btn-flat">{{ trans('messages.cancelarBoton') }}</button>
</div>

<!-- Aceptar submit -->
<div class="col-xs-6">
  <button id="botonEnviarRegistro" type="button" class="btn btn-primary btn-block btn-flat">{{ trans('messages.aceptarBoton') }}</button>
</div>

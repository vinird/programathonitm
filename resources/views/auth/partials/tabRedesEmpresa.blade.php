<p class="login-box-msg">{{ trans('messages.registerEmpresa') }}</p>

<!-- Facebook -->
<div class="form-group has-feedback">
  <input id="facebookEmpresa" type="text" class="form-control" placeholder="{{ trans('messages.facebookEmpresa') }}" name="facebookEmpresa" value="{{ old('facebookEmpresa') }}" required maxlength="300"/>
  <span class="fa fa-facebook form-control-feedback"></span>
</div>

<!-- Twitter -->
<div class="form-group has-feedback">
  <input type="text" class="form-control" placeholder="{{ trans('messages.twitterEmpresa') }}" name="twitterEmpresa" value="{{ old('twitterEmpresa') }}" maxlength="300"/>
  <span class="fa fa-twitter form-control-feedback"></span>
</div>

<!-- LinkedIn -->
<div class="form-group has-feedback">
  <input type="text" class="form-control" placeholder="{{ trans('messages.linkedinEmpresa') }}" name="linkedinEmpresa" value="{{ old('linkedinEmpresa') }}" maxlength="300"/>
  <span class="fa fa-linkedin form-control-feedback"></span>
</div>

<!-- Youtube -->
<div class="form-group has-feedback">
  <input type="text" class="form-control" placeholder="{{ trans('messages.youtubeEmpresa') }}" name="youtubeEmpresa" value="{{ old('youtubeEmpresa') }}" maxlength="300"/>
  <span class="fa fa-youtube form-control-feedback"></span>
</div>

<!-- Website -->
<div class="form-group has-feedback">
  <input type="text" class="form-control" placeholder="{{ trans('messages.webEmpresa') }}" name="webEmpresa" value="{{ old('webEmpresa') }}" maxlength="300"/>
  <span class="glyphicon glyphicon-globe form-control-feedback"></span>
</div>

<!-- CorreoEmpresa -->
<div class="form-group has-feedback">
  <input id="correoEmpresa" type="text" class="form-control" placeholder="{{ trans('messages.correoEmpresa') }}" name="correoEmpresa" value="{{ old('correoEmpresa') }}" maxlength="50"/>
  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
</div>

<!-- Atras Btn -->
<div class="col-xs-6">
  <button id="botonEmpresaAnt" type="button" class="btn btn-primary btn-block btn-flat">{{ trans('messages.atrasBoton') }}</button>
</div>

<!-- Siguiente Btn -->
<div class="col-xs-6">
  <button id="botonRedesSig" type="button" class="btn btn-primary btn-block btn-flat">{{ trans('messages.siguienteBoton') }}</button>
</div>

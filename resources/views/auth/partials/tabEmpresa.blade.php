<!-- Tab de empresa -->
<p class="login-box-msg">{{ trans('messages.registerEmpresa') }}</p>

<!-- NombreComercio  -->
<div class="form-group has-feedback">
  <input id="nombreComercial" type="text" class="form-control" placeholder="{{ trans('messages.nombreComercial') }}" name="nombreComercial" value="{{ old('nombreComercial') }}" maxlength="100" required/>
  <span class="glyphicon glyphicon-briefcase form-control-feedback"></span>
</div>

<!-- Pais -->
<div class="form-group has-feedback">
  <label for="paisEmpresa">{{ trans('messages.paisEmpresa') }}</label>
  <select id="pais" class="form-control" name="pais" required>
    <option value=""></option>
    @if(isset($paises))
    @if(count($paises) > 0)
    @foreach($paises as $pais)
    <option value="{{ $pais->Id }}">{{ $pais->Nombre}}</option>
    @endforeach
    @endif
    @endif
  </select>
  <span class="glyphicon glyphicon-globe form-control-feedback select-glyphicon"></span>
</div>

<!-- Estado -->
<div class="form-group has-feedback">
  <label for="provinciaEmpresa">{{ trans('messages.provinciaEmpresa') }}</label>
  <select id="provincia" class="form-control" name="provincia" required>
    <option value=""></option>
    @if(isset($estados))
    @if(count($estados) > 0)
    @foreach($estados as $estado)
    <option value="{{ $estado->Id }}">{{ $estado->Nombre}}</option>
    @endforeach
    @endif
    @endif
  </select>
  <span class="glyphicon glyphicon-globe form-control-feedback select-glyphicon"></span>
</div>

<!-- Sector -->
<div class="form-group has-feedback">
  <label for="sectorEmpresa">{{ trans('messages.sectorEmpresa') }}</label>
  <select id="sector" class="form-control" name="sector" required>
    <option value=""></option>
    @if(isset($sectores))
    @if(count($sectores) > 0)
    @foreach($sectores as $sector)
    <option value="{{ $sector->Id }}">{{ $sector->Nombre}}</option>
    @endforeach
    @endif
    @endif
  </select>
  <span class="glyphicon glyphicon-briefcase form-control-feedback select-glyphicon"></span>
</div>

<!-- AnnoInicioOperaciones -->
<div class="form-group has-feedback">
  <label for="annoEmpresa">{{ trans('messages.annoEmpresa') }}</label>
  <select id="annoEmpresa" class="form-control" name="annoEmpresa" required>
    <option value=""></option>
    @if(isset($annos))
    @if(count($annos) > 0)
    @foreach($annos as $anno)
    <option value="{{ $anno }}">{{ $anno }}</option>
    @endforeach
    @endif
    @endif
  </select>
  <span class="glyphicon glyphicon-calendar form-control-feedback select-glyphicon"></span>
</div>

<!-- Genero -->
<div class="form-group has-feedback">
  <label for="generoPropietario">{{ trans('messages.generoPropietario') }}</label>
  <select id="generoPropietario" class="form-control" name="generoPropietario" required>
    <option value=""></option>
    @if(isset($generos))
    @if(count($generos) > 0)
    @foreach($generos as $genero)
    <option value="{{ $genero->Id }}">{{ $genero->Nombre}}</option>
    @endforeach
    @endif
    @endif
  </select>
  <span class="glyphicon glyphicon-user form-control-feedback select-glyphicon"></span>
</div>

<!-- CedJuridica -->
<div class="form-group has-feedback">
  <input id="cedulaEmpresa" type="text" class="form-control" placeholder="{{ trans('messages.cedulaEmpresa') }}" name="cedulaEmpresa" value="{{ old('cedulaEmpresa') }}" required maxlength="50"/>
  <span class="glyphicon glyphicon-briefcase form-control-feedback"></span>
</div>

<!-- telefonoEmpresa -->
<div class="form-group has-feedback">
  <input id="telefonoEmpresa" type="text" class="form-control" placeholder="{{ trans('messages.telefonoEmpresa') }}" name="telefonoEmpresa" value="{{ old('telefonoEmpresa') }}" required maxlength="50"/>
  <span class="glyphicon glyphicon-phone form-control-feedback"></span>
</div>

<!-- Direccion -->
<div class="form-group has-feedback">
  <textarea id="direccionEmpresa" class="form-control" placeholder="{{ trans('messages.direccionEmpresa') }}" name="direccionEmpresa" value="{{ old('direccionEmpresa') }}" maxlength="200" required></textarea>
  <span class="glyphicon glyphicon-map-marker form-control-feedback"></span>
</div>

<!-- EsNegocioFamiliar -->
<div class="form-group has-feedback">
  <input id="negocioFamiliar" type="checkbox" class="form-control" name="negocioFamiliar" disabled required/>
  <label for="negocioFamiliar">{{ trans('messages.negocioFamiliar') }}</label>
</div>

<!-- Logo -->
<div class="form-group has-feedback">
  <label for="logoEmpresa">{{ trans('messages.logoEmpresa') }}</label>
  <input id="logoEmpresa" type="file" class="form-control" name="logoEmpresa" required accept=".png, .gif"/>
</div>

<!-- Siguiente Btn -->
<div class="col-xs-12">
  <button id="botonEmpresaSig" type="button" class="btn btn-primary btn-block btn-flat">{{ trans('messages.siguienteBoton') }}</button>
</div>

@extends('layouts.auth')

@section('htmlheader_title')
    Log in
@endsection

@section('content')
<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="{{ url('/home') }}"><b>La Voz</b> del Cliente</a>
        </div><!-- /.login-logo -->

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (session()->has('flash_notification.message'))
    <div class="alert alert-{{ session('flash_notification.level') }}">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>

        {!! session('flash_notification.message') !!}
    </div>
    @endif

    <div class="login-box-body">
    <p class="login-box-msg"> {{ trans('adminlte_lang::message.siginsession') }}</p>
    <form action="{{ url('/loginUser') }}" method="post">
        {!! csrf_field() !!}
        <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Nombre comercial" name="nombre_comercial" required maxlength="100" autofocus>
            <span class="fa fa-briefcase form-control-feedback" aria-hidden="true"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Nombre de usuario" name="usuario" required maxlength="50"/>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <span>Seleccionar País:</span>
            <select class="form-control" name="pais" required>
                <option></option>
              @if(isset($paises))
                @if(count($paises) > 0)
                    @foreach ($paises as $pais)
                        <option value="{{ $pais->Id }}"> {{ $pais->Nombre }} </option>
                    @endforeach
                @endif
              @endif
            </select>
            <span class="form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="{{ trans('adminlte_lang::message.password') }}" name="contrasena" required minlength="8" maxlength="10"/>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        </div>
        <!--  -->

        <div class="row">
            <div class="col-xs-4">
                <a href="/" class="btn btn-default btn-block btn-flat">Cancelar</a>
            </div><!-- /.col -->
            <div class="col-xs-8">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Ingresar</button>
            </div><!-- /.col -->
        </div>
    </form>


    <a data-toggle="modal" data-target="#ModalOlvidoContrasena" href="">{{ trans('adminlte_lang::message.forgotpassword') }}</a><br>
    <a href="{{ url('/register') }}" class="text-center">Registrarse</a>

</div><!-- /.login-box-body -->

</div><!-- /.login-box -->

    @include('layouts.partials.scripts_auth')

    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
</body>

@endsection


<div class="modal fade" id="ModalOlvidoContrasena" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Nuevo Mensaje</h4>
      </div>
      <div class="modal-body">
        <form action="mailto:someone@example.com">
          <div class="form-group">
            <label for="recipient-name" class="control-label">Correo:</label>
            <input type="text" class="form-control" id="recipient-name">
          </div>
          <div class="form-group">
            <label for="message-text" class="control-label">Mensaje:</label>
            <textarea class="form-control" id="message-text"></textarea>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Enviar</button>
        </form>
      </div>
    </div>
  </div>
</div>

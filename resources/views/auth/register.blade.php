@extends('layouts.auth')

@section('htmlheader_title')
    Register
@endsection

@section('content')

    <body class="hold-transition register-page">
    <div class="register-box">
      <div class="register-logo">
        <a href="{{ url('/home') }}"><b>La Voz</b> del Cliente</a>
      </div>

      @if (count($errors) > 0)
        <div class="alert alert-danger">
          <strong>Whoops!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
            <ul>
              @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
              @endforeach
            </ul>
        </div>
      @endif

      @if (session()->has('flash_notification.message'))
      <div class="alert alert-error alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> {!! session('flash_notification.message') !!}
      </div>
      @endif

      <div id="messageErrors" class="alert alert-danger hidden">
        <strong>Whoops!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
          <ul id="listErrors">
          </ul>
      </div>

      <ul class="nav nav-tabs">
        <li class="active"><a id="tabEmpresa" data-toggle="tab" href="#empresa">{{ trans('messages.tabEmpresa') }}</a></li>
        <li class="disabled"><a id="tabRedes" data-toggle="" href="#redes">{{ trans('messages.tabRedes') }}</a></li>
        <li class="disabled"><a id="tabUsuario" data-toggle="" href="#usuario">{{ trans('messages.tabUsuario') }}</a></li>
      </ul>

      <div class="register-box-body">
        <form id="formRegistro" action="{{ url('/register') }}" enctype="multipart/form-data" method="post">
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <div class="tab-content">
            <div id="empresa" class="tab-pane fade in active">
              @include('auth.partials.tabEmpresa')
            </div>
            <div id="redes" class="tab-pane fade">
              @include('auth.partials.tabRedesEmpresa')
            </div>
            <div id="usuario" class="tab-pane fade">
              @include('auth.partials.tabUsuario')
            </div>
          </div>
        </form>
        <br><br><br>
        <a href="{{ url('/getLoginUsuario') }}" class="text-center">{{ trans('messages.membreship') }}</a>
      </div><!-- /.form-box -->
    </div><!-- /.register-box -->

    @include('layouts.partials.scripts_auth')

    @include('auth.terms')

    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
</body>

@endsection

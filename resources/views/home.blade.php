@extends('layouts.app')

@section('htmlheader_title')
Home
@endsection


@section('main-content')
<!-- Vista para iniciar Facebook -->
<!--  -->
<div class="container-fluid spark-screen">
	<div class="row" >
		<div class="col-xs-12">
			<div class="panel panel-default">


				
				@if(isset($pyme))
				@if(count($pyme) > 0)
				@if($pyme->EsFacebookAppInstalado)


				<div class="panel-heading" >
					<div class="form-group">
						<label>Rango de fechas:</label>
						<div class="input-group">
							<div class="input-group-addon">
								<i class="fa fa-calendar"></i>
							</div>
							<input type="text" class="form-control pull-right" name="daterange" value="" />
						</div>
						<!-- /.input group -->
						<label>Enlace de la encuesta:</label>

						<div class="input-group">
							<div class="input-group-addon">
								<i class="fa fa-facebook" style="cursor: pointer;"></i>
							</div>
							<a class="form-control pull-right" target="_blank" href="{{$enlaceEncuesta}}">
								{{ trans('facebook.compartir') }}
							</a>
						</div>


						<script type="text/javascript">
							function select_all(obj) {
								var text_val=eval(obj);
								text_val.focus();
								text_val.select();
					            if (!document.all) return; // IE only
					            r = text_val.createTextRange();
					            r.execCommand('copy');
					        }
					    </script>

					    <!-- /.input group -->
					</div>
				</div>

				<div class="panel-body">
					@include('layouts.partials.graficas')
				</div>

				@else
				@include('facebook.instalarApp')
				@endif
				@endif
				@endif




			</div>
		</div>
	</div>
</div>
@endsection

@extends('layouts.auth')

@section('htmlheader_title')
    Register
@endsection

@section('content')

    <!-- Vista para iniciar Facebook -->

    <body class="hold-transition register-page">
    <div class="register-box">
        <div class="register-logo">
            {{--todo: paguina facebook pyme y nombre pyme--}}
            <a href="{{ url('/home') }}"><b>{{'data'.nombre}}</b></a>
        </div>

        <div class="register-box-body">

            <a href="{{  url('/testInit')  }}" class="button text-center">{{ trans('facebook.inicir') }}</a>
        </div><!-- /.form-box -->
    </div><!-- /.register-box -->

    {{--@include('layouts.partials.scripts_auth')--}}

    {{--@include('facebook.terminos')--}}

    </body>

@endsection


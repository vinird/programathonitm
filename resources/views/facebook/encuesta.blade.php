@extends('layouts.auth')

@section('htmlheader_title')
    Register
@endsection

@section('content')

<body class="hold-transition register-page">
<div class="register-box">
    <div class="register-logo">
        <h1>{{$nombre}}</h1>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="register-box-body">

        @include('facebook.pretuntas')

    </div><!-- /.form-box -->
</div><!-- /.register-box -->

@include('layouts.partials.scripts_auth')
@include('facebook.terminos')

</body>

@endsection

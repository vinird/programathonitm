
<form action="{{ url('/respuestas/'.$id) }}" method="post">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="form-group has-feedback">
        <p class="">A. {{trans('facebook.pregunta.a')}}</p>
        <select required title="a" name="A">
            @include('facebook.respuestas')
        </select>
    </div>

    <div class="form-group has-feedback">
        <p class="">B. {{trans('facebook.pregunta.b')}}</p>
        <select required title="b"  name="B">
            @include('facebook.respuestas')
        </select>
    </div>


    <div class="form-group has-feedback">
        <p class="">C. {{trans('facebook.pregunta.c')}}</p>
        <select required title="c"  name="C">
            @include('facebook.respuestas')
        </select>
    </div>


    <div class="form-group has-feedback">
        <p class="">D. {{trans('facebook.pregunta.d')}}</p>
        <select required title="d"  name="D">
            @include('facebook.respuestas')
        </select>
    </div>

    <div class="form-group has-feedback">
        <p class="">E. {{trans('facebook.pregunta.e')}}</p>
        <select required title="e"  name="E">
            @include('facebook.respuestas')
        </select>
    </div>



    <div class="form-group has-feedback">
        <p class="">F. {{trans('facebook.pregunta.f')}}</p>
        <select required title="f"  name="F">
            <option disabled selected value>{{ trans('facebook.respuestas.seleccionar') }}</option>
            <option value="1">{{ trans('facebook.edades.1') }}</option>
            <option value="2">{{ trans('facebook.edades.2') }}</option>
            <option value="3">{{ trans('facebook.edades.3') }}</option>
            <option value="4">{{ trans('facebook.edades.4') }}</option>
            <option value="5">{{ trans('facebook.edades.5') }}</option>
            <option value="6">{{ trans('facebook.edades.6') }}</option>
            <option value="7">{{ trans('facebook.edades.7') }}</option>
        </select>
    </div>
    <div id="facebookData" class="form-group has-feedback hidden">
        <div class="form-group has-feedback">
            <p class="">G. {{trans('facebook.pregunta.g')}}</p>
            <select required title="g"  id="genero" name="G">
                <option disabled selected value>{{ trans('facebook.respuestas.seleccionar') }}</option>
                <option value="M">{{ trans('facebook.genero.M') }}</option>
                <option value="F">{{ trans('facebook.genero.F') }}</option>
                <option selected="selected" value="N">{{ trans('facebook.genero.N') }}</option>
            </select>
        </div>
    </div>

    {{--enviar formulario--}}
    <div class="row">
        <div class="col-xs-1">
            <label>
                <div class="checkbox_register icheck">
                    <label>
                        <input required type="checkbox" name="terms">
                    </label>
                </div>
            </label>
        </div><!-- /.col -->
        <div class="col-xs-6">
            <div class="form-group">
                <button type="button" class="btn btn-block btn-flat" data-toggle="modal" data-target="#termsModal">{{ trans('adminlte_lang::message.terms') }}</button>
            </div>
        </div><!-- /.col -->
        <div class="col-xs-4 col-xs-push-1">
            <button type="submit" class="btn btn-primary btn-block btn-flat">{{ trans('facebook.enviar') }}</button>
        </div><!-- /.col -->
    </div>
    {{--fin enviar formulario--}}

    @include('facebook.facebookInit')

</form>
<link href="{{ asset('/css/instalarApp.css') }}" rel="stylesheet">

<h3 id="titulo">{{ trans('facebook.porFavorCompartir') }}</h3>

{{--<button id="compartirEncuesta" class="btn btn-primary">{{ trans('facebook.compartirEncuestaEnFacebook') }}<i class="fa fa-plus" aria-hidden="true"></i></button>--}}


<a id="compartirEncuesta" class="btn btn-primary" target="_blank" href="{{$enlaceEncuesta}}">
    {{ trans('facebook.compartir') }}
</a>


<script>
    jQuery(document).ready(function($) {
        $('#compartirEncuesta').click( function () {
            $('#idPyme_input').val(idPyme);
            $('#instalar-form').submit();
        });
    });
</script>

<form class="hidden" id="instalar-form" action="{{ url('/activeFaceBookApp') }}" method="POST">
    {{ csrf_field() }}
    <input id="idPyme_input" type="text"  value="" name="idPyme">
</form>


{{--console.log(idPyme);--}}
{{--document.getElementById('instalar-form').submit();--}}

<script>
	jQuery(document).ready(function($) {
	// Carga el SDK de Facebook
	window.fbAsyncInit = function() {
		FB.init({ 
			appId: '1783984441885181',
			status: true, 
			cookie: true, 
			xfbml: true,
			version: 'v2.7'
		});
		FB.getLoginStatus(function(response) {
			if (response.status === 'connected') {
				console.log('Conected');
                optenerGenero();
			}
			else {
				console.log('Disconected');
                facebookConectar();
		}
	});
	};
	(function(d, s, id){
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) {return;}
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	// ///////////////////////////////

	// Inicia el login de Facebook
	function facebookConectar(){
		$('html').dimmer({closable: false}).dimmer('toggle');
		FB.login(function(response) {
			if (response.authResponse) {
				console.log('Welcome!  Fetching your information.... ');
				FB.api('/me', function(response) {
					console.log('Good to see you, ' + response.name + '.');
                });
				$('html').dimmer('toggle');

                optenerGenero();
			} else {
				console.log('User cancelled login or did not fully authorize.');
                //Toggle Hidden
                $('#facebookData').toggleClass('hidden');
                //todo: remove
                $('#genero').val("N");
                $('html').dimmer('toggle');
			}
		});
	}
	/////////////////////

        {{--<option value="M">{{ trans('facebook.genero.M') }}</option>--}}
        {{--<option value="F">{{ trans('facebook.genero.F') }}</option>--}}
        {{--<option value="N">{{ trans('facebook.genero.N') }}</option>--}}
    //todo:Optener el genero y marcar la opcion
    function optenerGenero() {

        FB.api('/me','GET',{"fields":"name,age_range,gender"},
            function(response) {
                switch( response.gender ){
                    case "male":
                        $('#genero').val("M");
                    break;
                    case "female":
                        $('#genero').val("F");
                        break;
                    default:
                        $('#genero').val("N");
                }
                console.log($('#genero').val());
            }
        );
    }

});
</script>
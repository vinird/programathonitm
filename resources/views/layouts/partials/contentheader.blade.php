<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        @yield('contentheader_title', 'Panel de Métricas - '.$pyme->NombreComercio) 
        @if(isset($pymes))
        @if(count($pymes) > 0)
        		<a href="/pyme/{{ $pyme->Id }}/edit" class="btn btn-warning btn-xs"> <i class="fa fa-pencil" aria-hidden="true"></i></a>	
        @endif
        @endif
        <small>@yield('contentheader_description')</small>
    </h1>
    <ol class="breadcrumb">
        <li><i class="fa fa-line-chart" aria-hidden="true"></i> Métricas</li>
    </ol>
</section>
<div class="row">
  <div class="col-xs-12 col-md-6">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Comportamiento de respuestas por pregunta</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body" >
        <div class="chart">
          <canvas id="chartRespuestas" style="height: 250px; width: 415px;" height="250" width="415"></canvas>
        </div>
      </div>
      <!-- /.box-body -->
    </div>
  </div>

  <div class="col-xs-12 col-md-6">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Datos del género de los votantes</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <div class="chart">
          <canvas id="chartGenero" style="height: 250px; width: 415px;" height="250" width="415"></canvas>
        </div>
      </div>
      <!-- /.box-body -->
    </div>
  </div>

</div>
<br>
<br>
<div class="row">
  <div class="col-xs-12 col-md-6">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Rango de edad de los votantes</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <div class="chart">
          <canvas id="chartEdad" style="height: 250px; width: 415px;" height="250" width="415"></canvas>
        </div>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
</div>
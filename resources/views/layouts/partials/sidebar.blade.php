<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (!session()->get('key'))
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="data:image/jpeg;base64,{{ base64_encode($pyme->Logo) }}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>
                        @if(isset($usuario))
                            @if(count($usuario) > 0)                                        
                                {{$usuario -> Usuario}}
                            @endif
                        @endif
                    </p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('adminlte_lang::message.online') }}</a>
                </div>
            </div>
        @endif

        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="{{ trans('adminlte_lang::message.search') }}..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">Menú</li>
            <!-- Optionally, you can add icons to the links -->
            {{--<li class="active">--}}
                {{--<a href="{{ url('home') }}">--}}
                    {{--<i class="fa fa-plus" aria-hidden="true"></i> --}}
                    {{--<span>Crear Encuesta</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-bar-chart" aria-hidden="true"></i>
                    <span>Ver resultados</span>
                    <i class="fa fa-chevron-down pull-right"></i>
                </a>
                {{--todo:listar Pymes--}}
                <ul class="treeview-menu">
                    @foreach ($pymes as $p)
                        <li><a href="{{url('/home/'.$p->Id)}}">{{$p->NombreComercio}}</a></li>
                    @endforeach
                </ul>
            </li>


            <li class="treeview">
                <a href="#">
                    <i class="fa fa-pencil" aria-hidden="true"></i> 
                    <span>Modifcar 
                        @if(isset($pyme))
                            @if(count($pyme) > 0)
                                {{$pyme->NombreComercio}}
                            @endif
                        @endif
                    </span> 
                    <i class="fa fa-chevron-down pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <!-- <li><a href="#">Poner en inactiva/activa</a></li> -->
                    @if(isset($pymes))
                    @if(count($pymes) > 0)
                        <li><a href="/pyme/{{ $pyme->Id }}/edit">Modificar</a></li>
                    @endif
                    @endif
                </ul>
            </li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>

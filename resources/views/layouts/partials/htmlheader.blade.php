<head>
  <meta charset="UTF-8">
  <title> Panel de Métricas - 
    @if(isset($pyme))
    @if(count($pyme) > 0)
    {{$pyme->NombreComercio}}
    @endif
    @endif | La Voz del Cliente
  </title>
<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
  
  <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
  <!-- Bootstrap 3.3.4 -->
  <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet" type="text/css" />
  <!-- Font Awesome Icons -->
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <!-- Ionicons -->
  <link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
  <!-- Theme style -->
  <link href="{{ asset('/css/AdminLTE.css') }}" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
          page. However, you can choose any other skin. Make sure you
          apply the skin class to the body tag so the changes take effect.
        -->
        <link href="{{ asset('/css/skins/skin-green.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- iCheck -->
        <link href="{{ asset('/plugins/iCheck/square/blue.css') }}" rel="stylesheet" type="text/css" />

        <!-- dimmer -->
        <link href="{{ asset('/css/dimmer.css') }}" rel="stylesheet" type="text/css" />

        <!-- Hoa general de estilos -->
        <link href="{{ asset('/css/general.css') }}" rel="stylesheet" type="text/css" />

        <link href="{{ asset('/plugins/daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="{{ asset('/js/jquery.min.js') }}"></script>
    <script src="{{ asset('/js/angular.min.js') }}"></script>
    <script src="{{ asset('/js/angular-route.min.js') }}"></script>
    <script src="{{ asset('/js/dimmer.js') }}"></script>
    <script src="{{ asset('/js/validationRegister.js') }}"></script>
    <script src="{{ asset('/plugins/chartjs/Chart.min.js') }}"></script>
    
    @if(isset($pyme))
    @if(count($pyme) > 0)
    @if($pyme->EsFacebookAppInstalado)
      <script src = "{{ asset('/js/controllers/graficasCtrl.js') }}"></script>
    @endif
    @endif
    @endif




    <script src = "{{ asset('/plugins/daterangepicker/moment.min.js') }}"></script>
    <script src = "{{ asset('/plugins/daterangepicker/daterangepicker.js') }}"></script>

  </head>

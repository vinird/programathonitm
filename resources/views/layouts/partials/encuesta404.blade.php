<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="La Voz del Cliente ">
    <meta name="author" content="ITM">
	<title>Encuesta Exitosa | <b>La Voz</b> del Cliente</title>

	<!-- estilos -->
	<link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet">
	<link href="{{ asset('/css/encuesta404.css') }}" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,300,700' rel='stylesheet' type='text/css'>
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <script src="{{ asset('/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
</head>
<body>
	<!-- Fixed navbar -->
	<header>
		<div id="navigation" class="navbar navbar-inverse navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#"><b>La Voz</b> del Cliente</a>
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li class="active"><a href="/" class="smoothScroll">{{ trans('adminlte_lang::message.home') }}</a></li>
					</ul>
					<ul class="nav navbar-nav navbar-right">
						@if (!session()->get('activo'))
						<li><a href="{{ url('/getLoginUsuario') }}">{{ trans('adminlte_lang::message.login') }}</a></li>
						<li><a href="{{ url('/register') }}">{{ trans('adminlte_lang::message.register') }}</a></li>
						@else
						<li><a href="/home">{{ session()->get('nombreCompleto') }}</a></li>
						@endif
					</ul>
				</div><!--/.nav-collapse -->
			</div>
		</div>
	</header>
	<main>
		<div id="headerwrap">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 centered">
                    <h2>Oops! Encuesta no encontrada</b></h2>
                    <h3>Vuelve a intentarlo más tarde.</h3>                    
                    <i class="fa fa-frown-o" aria-hidden="true" id="sadFace"></i>
                </div>
            </div>
        </div><!--/ .container -->
    </div><!--/ #headerwrap --> 
	</main>

	<footer>
		
	</footer>

    <!-- Bootstrap core JavaScript -->
    <script src="{{ asset('/js/bootstrap.min.js') }}" type="text/javascript"></script>
	
</body>
</html>
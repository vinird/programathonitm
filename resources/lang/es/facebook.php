<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'inicir' => 'Iniciar encuesta',
    'enviar' => 'Votar',
    'compartir' => 'Compartir en Facebook',
    'porFavorCompartir' => 'Para visualizar las métricas, por favor comparta el enlace a la encuesta',
    'compartirEncuestaEnFacebook' => 'Compartir encuesta en Facebook',

    'respuestas' => [
        'seleccionar' => ' -- Seleccione una opción -- ',
        'excelente' => 'Excelente',
        'bueno' => 'Bueno(a)',
        'normal' => 'Normal',
        'regular' => 'Regular',
        'malo' => 'Malo',
    ],


    'pregunta' => [
        'a' => '¿Calidad del producto o servicios?',
        'b' => '¿Tiempo de espera en la atención?',
        'c' => '¿Imagen de las instalaciones?',
        'd' => '¿Disponibilidad de producto o servicio solicitado?',
        'e' => '¿Atención del personal?',
        'f' => '¿A qué grupo de edad pertenece?',
        'g' => '¿Cuál es su sexo?',
    ],

    'edades' => [
        '1' => '12-17',
        '2' => '18-33',
        '3' => '34-45',
        '4' => '46-55',
        '5' => '56-64',
        '6' => '65-73',
        '7' => '74+',
    ],

    'genero' => [
        'M' => 'Masculino',
        'F' => 'Femenino',
        'N' => 'No Aplica',
    ],
];

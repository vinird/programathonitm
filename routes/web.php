<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'CheckUsuario'], function () {
	Route::get('/home/{id?}', 'HomeController@index');
});


// Route::get('/admin/main', [ 'uses' => 'Main@index' , 'middleware' => ['auth', 'userActive']])->name('admin.main');


// Rutas de sistema para usuarios
Route::resource('/usuario', 'Usuarios');
Route::get('/getLoginUsuario', 'Usuarios@getLogin');
Route::post('/loginUser', 'Usuarios@loginUser');
Route::post('/logOut', 'Usuarios@logOut');

// Rutas de sistema para TipoRedSocial
Route::resource('/tipoRedSocial', 'TipoRedSociales');

// Rutas de sistema para Sector
Route::resource('/sectores', 'Sectores');

// Rutas de sistema para Respuesta
//Route::resource('/respuestas', 'Respuestas');
Route::get('/encuesta', 'Respuestas@redireccionar');
Route::get('/encuesta/{id}', 'Respuestas@index');
Route::post('/respuestas/{id}', 'Respuestas@store');

// Rutas de sistema para RedSocial
Route::resource('/redSocial', 'RedSociales');

// Rutas de sistema para Pyme
Route::resource('/pyme', 'Pymes');
Route::post('/activeFaceBookApp', 'Pymes@activeFaceBookApp');

// Rutas de sistema para Pais
Route::resource('/pais', 'Paises');

// Rutas de sistema para Genero
Route::resource('/genero', 'Generos');

// Rutas de sistema para Estado
Route::resource('/estado', 'Estados');

// Ruta para mostrar mensaje de confirmación al enviar la encuesta
Route::get('/encuestaEnviada', function () {
    return view('/layouts/partials/encuestaExitosa');
});

// Ruta para mostrar mensaje al no encontrar la encuesta
Route::get('/encuesta404', function () {
    return view('/layouts/partials/encuesta404');
});
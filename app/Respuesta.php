<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Respuesta extends Model
{
    protected $table = 'respuesta';
    public $timestamps = false;

    protected $fillable = [
        'ID', 'Respuesta01', 'Respuesta02', 'Respuesta03', 'Respuesta04', 'Respuesta05', 'FechaRespuesta', 'GeneroID', 'Campo01', 'Campo02', 'RangoEdad', 'PymeID'
    ];
}

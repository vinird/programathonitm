<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    protected $table = 'pais';
    public $timestamps = false;
    protected $fillable = [
        'Id', 'Nombre', 'Iso', 'Iso3', 'CodigoNumerico', 'CodigoTelefono',
    ];
}

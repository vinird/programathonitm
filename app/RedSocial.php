<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RedSocial extends Model
{
    protected $table = 'redsocial';

    public $timestamps = false;
    protected $fillable = [
        'TipoRedSocialID', 'Comentario', 'InformacionContacto', 'PymeID', 'Link',
    ];
}

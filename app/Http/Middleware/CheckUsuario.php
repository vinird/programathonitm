<?php

namespace App\Http\Middleware;

use Closure;

class CheckUsuario
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session()->get('activo')){
            return $next($request);
        }
        session()->flush();
        return redirect('/getLoginUsuario');
    }
}

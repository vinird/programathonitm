<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Http\Requests;

use File;
use DB;
use App\Usuario;
use App\Pais;
use App\Estado;
use App\Genero;
use App\Sector;
use App\Pyme;

use Carbon\Carbon;

class Pymes extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function index()
  {
    //
  }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function activeFaceBookApp(Request $request)
    {
      $pyme = Pyme::find($request->idPyme);

      DB::table('pyme')->where('id', $request->idPyme)->update(['EsFacebookAppInstalado' => 1]);

      // $pyme->EsFacebookAppInstalado = 1;
      $pyme->save();
      return back();
    }


  /**
  * Show the form for creating a new resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function create()
  {
    //
  }

  /**
  * Store a newly created resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @return \Illuminate\Http\Response
  */
  public function store(Request $request)
  {
    //
  }

  /**
  * Display the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function show($id)
  {
    //
  }

  /**
  * Show the form for editing the specified resource.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function edit($id)
  {
    //Cargando Datos para formulario
    $paises=Pais::all();
    $estados=Estado::all();
    $sectores=Sector::all();
    $generos=Genero::all();
    $dt = Carbon::now();
    $annos=  range($dt->year, 1900 ,-1);
    $pyme = Pyme::find($id);
    $miEstado= Estado::find($pyme->EstadoID);
    $miPais= Pais::find($miEstado->PaisID);
    //Enviando datos a formulario y cargando el mismo
    return view('modificarPyme')->with(['pyme' => $pyme,'paises' => $paises, 'estados' => $estados, 'sectores' => $sectores, 'generos' => $generos, 'annos'=>$annos, 'miPais'=>$miPais]);
  }

  /**
  * Update the specified resource in storage.
  *
  * @param  \Illuminate\Http\Request  $request
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function update(Request $request, $id)
  {
    $this->validate($request, [
      'pais'                => 'required',
      'provincia'           => 'required',
      'sector'              => 'required',
      'annoEmpresa'         => 'required',
      'generoPropietario'   => 'required',
      'cedulaEmpresa'       => 'required|max:50',
      'telefonoEmpresa'     => 'required|max:50',
      'direccionEmpresa'    => 'required|max:200',
      'logoEmpresa'         => 'mimes:gif,png|max:50',
    ]);

    $pymes=Pyme::all();
    $usuarios=Usuario::all();
    $exist=false;

    // Se asignan valores a la pyme
    $date= substr(strval($request->annoEmpresa),2);//Consigue los 2 ultimos digitos de año
    DB::table('pyme')->where('id', $id)->update(['EstadoID' => $request->provincia,
                                                  'SectorID' => $request->sector,
                                                  'AnnoInicioOperaciones'=> intval($date),
                                                  'NumeroTelefono' => $request->telefonoEmpresa,
                                                  'Direccion' => $request->direccionEmpresa,
                                                  'EsActiva' => ($request->pymeActiva)? 1:0,
                                                  'EsNegocioFamiliar' => ($request->negocioFamiliar)? 1:0,
                                                  'GeneroPropietarioID' => $request->generoPropietario,
                                                  'CedJuridica' => $request->cedulaEmpresa,
                                                  'FechaUltimaActualizacion' => Carbon::now()]);

    //Actualizando datos del LogoEmpresa en caso de ser insertado
    $archivo = Input::file('logoEmpresa');
    if ($archivo !== null) {
      $datosImagen = file_get_contents($archivo);
      DB::table('pyme')->where('id', $id)->update(['Logo' => $datosImagen,
      'ExtensionLogo' => File::extension($request->logoEmpresa->getClientOriginalName())]);
    }

    //Regresa al formulario de edicion
    return back();
  }

  /**
  * Remove the specified resource from storage.
  *
  * @param  int  $id
  * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
    //
  }
}

<?php

namespace App\Http\Controllers;

use App\Respuesta;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Lang;
use App\Pyme;
use Carbon\Carbon;
class Respuestas extends Controller
{

    /**
     * Verificar si la Pyme tiene habilitada las encuestas
     * @param \App\Pyme  $pyme
     * @return bool
     */
    public function verificarDisponileEncuesta( $pyme ){
        return $pyme && $pyme->EsActiva && $pyme->EsFacebookAppInstalado;
    }

    public function index($id)
    {
        $pyme = Pyme::find($id);
        if( $this->verificarDisponileEncuesta($pyme)){
            //la encuesta esta activa optener los datos necesarios para la Vista
//            $data = [
//                'nombre' => 'Nombre Prueba',
//                'url' => 'facebook.com',
//                'id' => $id,
//            ];

            $data = [
                'nombre' => $pyme->NombreComercio,
                'id' => $id,
            ];
//            dd($data);
            return view('facebook/encuesta', $data);
        }else{
            //la encuesta no esta disponible redirir
            return redirect('/encuesta404');
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $pyme = Pyme::find($id);

        if( $this->verificarDisponileEncuesta($pyme)){
            //la encuesta esta activa optener los datos necesarios para la Vista

            //Validar cambos del formulario
//        'a' => '¿Calidad del producto o servicio?',
//        'b' => '¿Tiempo de espera en la atención?',
//        'c' => '¿Imagen de las instalaciones?',
//        'd' => '¿Disponibilidad de producto o servicio solicitado?',
//        'e' => '¿Atención del personal?',
//        'f' => '¿A qué grupo de edad pertenece?',
//        'g' => '¿Cuál es su sexo?',
            $this->validate($request, [
                'A' => 'required',
                'B' => 'required',
                'C' => 'required',
                'D' => 'required',
                'E' => 'required',
                'F' => 'required',
                'G' => 'required',
                'terms' => 'required',
            ]);

            $respuesta = new Respuesta();
            //'ID', 'Respuesta01', 'Respuesta02', 'Respuesta03', 'Respuesta04', 'Respuesta05', 'FechaRespuesta', 'GeneroID', 'Campo01', 'Campo02', 'RangoEdad', 'PymeID'

            $respuesta->PymeID = $id;
            $respuesta->Respuesta01 = $request->input('A');
            $respuesta->Respuesta02 = $request->input('B');
            $respuesta->Respuesta03 = $request->input('C');
            $respuesta->Respuesta04 = $request->input('D');
            $respuesta->Respuesta05 = $request->input('E');

            //Almacenar el rango de edad
            $respuesta->RangoEdad = $request->input('F');
            //Almacenar el genero
            $respuesta->GeneroID = $request->input('G');

            //almacenar la Fecha de la Respuesta
            $respuesta->FechaRespuesta = Carbon::now();

            //Guardar la respuesta
            $respuesta->save();

            return redirect('/encuestaEnviada');
        }else{
            //la encuesta no esta disponible redirir
            return redirect('/encuesta404');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Pais;

use App\Usuario;

use App\Pyme;

use App\Estado;

class Usuarios extends Controller
{
    /**
     * Log user
     *
     * @return \Illuminate\Http\Response
     */
    public function getLogin()
    {
        // Verifica si el usuario esta logeado en el sistema
        if (session()->get('activo')) {
            return redirect('/home');
        } else { 
            $paises = Pais::all();
            return view('auth.login')->with(['paises' => $paises]);
        }
    }

    /**
     * Log out
     *
     * @return \Illuminate\Http\Response
     */
    public function logOut()
    {
        // Limpia las variables de sesion y redirecciona a la raiz
        session()->flush();
        return redirect('/');
    }

    /**
     * Log user
     *
     * @return \Illuminate\Http\Response
     */
    public function loginUser(Request $request)
    {
        $this->validate($request, [
            'nombre_comercial'  => 'required|max:100',
            'usuario'           => 'required|max:50',
            'pais'              => 'required',
            'contrasena'        => 'required|max:10|min:8',
            ]);

        // Se obtienen los datos de la tabla usuario donde el campo Usuario es igual al usuario ingresado en el login
        $usuario = Usuario::where('usuario', '=', $request->usuario)->first();

        // Se valida si el usuario existe
        if($usuario){

            // Se obtienen los datos de la pyme donde el campo UsuarioID es igual al ID del usuario ingresado en el login
            $pymes = Pyme::where('UsuarioID', '=', $usuario->ID)->get();
            $estadoID;

            $existe = false;
            $pymeSeleccionada;
            // Se verifica si el nombre comercial ingresado pertenece a alguna de las pymes del usuario
            foreach ($pymes as $pyme) {
                if ($pyme->NombreComercio == $request->nombre_comercial) {
                    $estadoID = $pyme->EstadoID;
                    $existe = true;
                    $pymeSeleccionada = $pyme;
                }
            }
            // Se muestra mensaje de que el nombre comercial es incorrecto
            if(!$existe){
                flash('Nombre comercial equivocado.', 'danger');
                return back();
            }
            // Se obtiene el estado del EstadoID de la pyme ingresada
            $estado = Estado::find($estadoID);
            // Se obtiene el país a partir del PaisID del estado obtenido
            $pais = Pais::find($estado->PaisID);

            // Se muestra un mensaje de error al ingresar el pañis incorrecto
            if ($pais->Id != $request->pais) {
                $paisSeleccionado = Pais::find($request->pais);
                flash('Usted no tiene un comercio registrado en '.$paisSeleccionado->Nombre, 'danger');
                return back();
            }

            // Se muestra mensaje de error si la contraseña ingresada es incorrecta
            if ($usuario->Clave != $request->contrasena) {
                flash('La contraseña ingresada es incorrecta.', 'danger');
                return back();
            }

            // Variables de sesion
            session(['activo' => true]);
            session(['usuario' => $usuario->Usuario]);
            session(['nombreCompleto' => $usuario->NombreCompleto]);
            session(['email' => $usuario->EmailContacto]);
            session(['idPymeActual' => $pymeSeleccionada->Id]);
            session(['idUsuario' => $usuario->ID]);


            // Retorna la vista
            return redirect('/home');
//            return view('home')->with(['pyme' => $pymeSeleccionada, 'usuario' => $usuario]);

        } else {
            // Se muestra mensaje de error si el usuario ingresado es incorrecto
            flash('Este nombre de usuario no existe.', 'danger');
        }
        return back();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

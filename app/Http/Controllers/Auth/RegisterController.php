<?php

namespace App\Http\Controllers\Auth;

// use App\User;
// use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
// use Illuminate\Foundation\Auth\RegistersUsers;

use Illuminate\Http\Request;
use App\Http\Requests;

use Carbon\Carbon;

use File;

use App\Usuario;
use App\Pais;
use App\Estado;
use App\Sector;
use App\Genero;
use App\RedSocial;
use App\Pyme;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    // use RegistersUsers;

    public function showRegistrationForm(){
      //Cargando Datos para formulario
      $paises=Pais::all();
      $estados=Estado::all();
      $sectores=Sector::all();
      $dt = Carbon::now();
      $annos=  range($dt->year, 1900 ,-1);
      $generos=Genero::all();
      //Enviando datos a formulario y cargando el mismo
      return view('auth.register')->with(['paises' => $paises, 'estados' => $estados, 'sectores' => $sectores, 'generos' => $generos, 'annos' => $annos]);
    }


    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'     => 'required|max:255',
            'email'    => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function register(Request $request)
    {
        $pymes=Pyme::all();
        $usuarios=Usuario::all();
        $exist=false;
        foreach ($pymes as $pyme) {
          foreach ($usuarios as $usuario) {
            $miEstado= Estado::find($pyme->EstadoID);
            $miPais= Pais::find($miEstado->PaisID);
              if($pyme->NombreComercio==$request->nombreComercial && $usuario->Usuario== $request->username && $usuario->Clave== $request->password && $miPais->Id== $request->pais){
                $exist=true;
              }
          }
        }

        //Validaciones
        $this->validate($request, [
          'nombreComercial'       => 'required|max:100',
          'pais'                  => 'required',
          'provincia'             => 'required',
          'sector'                => 'required',
          'annoEmpresa'           => 'required',
          'generoPropietario'     => 'required',
          'cedulaEmpresa'         => 'required|max:50',
          'telefonoEmpresa'       => 'required|max:50',
          'direccionEmpresa'      => 'required|max:200',
          'facebookEmpresa'       => 'required|max:300',
          'correoEmpresa'         => 'email|max:50',
          'name'                  => 'required|max:50',
          'username'              => 'required|max:50',
          'password'              => 'required|min:8|max:10',
          'password_confirmation' => 'required|min:8|max:10',
          'logoEmpresa'           => 'required|mimes:gif,png|max:50',
          'email'                 => 'required|max:50|email',
          'email_confirmation'    => 'required|max:50|email'
        ]);

        if(!$exist) {
          // Se crea un instancia del modelo Usuario
          $usuario = new Usuario();
          // Se asignan valores al Usuario
          $usuario->Usuario = $request->username;
          $usuario->NombreCompleto = $request->name;
          $usuario->EmailContacto = $request->email;
          $usuario->Clave = $request->password;
          // Se guardan los nuevos datos de Usuario
          $usuario->save();

          // Se crea un instancia del modelo Pyme
          $pyme = new Pyme();
          // Se asignan valores a la Pyme
          $pyme->NombreComercio = $request->nombreComercial;
          $pyme->EstadoID = $request->provincia;
          $pyme->SectorID = $request->sector;
          $date= substr(strval($request->annoEmpresa),2);
          $pyme->AnnoInicioOperaciones = intval($date);
          $pyme->NumeroTelefono = $request->telefonoEmpresa;
          $pyme->Direccion = $request->direccionEmpresa;
          $pyme->EsActiva = 1 ;
          $pyme->EsNegocioFamiliar = 0;
          $archivo= Input::file('logoEmpresa');
          if($archivo !== null) {
            $pyme->Logo = file_get_contents($archivo);;
            $pyme->ExtensionLogo = File::extension($request->logoEmpresa->getClientOriginalName());
          }
          $pyme->EsFacebookAppInstalado= 0;
          $pyme->UsuarioID= Usuario::orderBy('ID', 'desc')->first()->ID;
          $pyme->GeneroPropietarioID = $request->generoPropietario;
          $pyme->CedJuridica = $request->cedulaEmpresa;
          $pyme->FechaCreacion= Carbon::now();
          $pyme->FechaUltimaActualizacion= Carbon::now();
          // Se guardan los nuevos datos de Pyme
          $pyme->save();

          // Se crea un instancia del modelo RedSocial Facebook
          $redSocialFb= new RedSocial();
          // Se asignan valores a la redSocial Facebook
          $redSocialFb->TipoRedSocialID =1;
          $redSocialFb->PymeID = $pyme->id;
          $redSocialFb->Link = $request->facebookEmpresa;
          // Se guardan los nuevos datos de redSocial Facebook
          $redSocialFb->save();

          // Se crea un instancia del modelo RedSocial Twitter
          $redSocialTwt= new RedSocial();
          // Se verifica si el Link de Twitter tiene datos asignan valores a la redSocial Twitter
          if(!empty($request->twitterEmpresa) || !ctype_space($request->twitterEmpresa)){
            $redSocialTwt->TipoRedSocialID =2;
            $redSocialTwt->PymeID = $pyme->id;
            $redSocialTwt->Link = $request->twitterEmpresa;
            // Se guardan los nuevos datos de la redSocial Twitter
            $redSocialTwt->save();
          }

          // Se crea un instancia del modelo RedSocial LinkedIn
          $redSocialIn= new RedSocial();
          // Se verifica si el Link de Twitter tiene datos asignan valores a la redSocial LinkedIn
          if(!empty($request->linkedinEmpresa) || !ctype_space($request->linkedinEmpresa)){
            $redSocialIn->TipoRedSocialID =3;
            $redSocialIn->PymeID = $pyme->id;
            $redSocialIn->Link = $request->linkedinEmpresa;
            // Se guardan los nuevos datos de la redSocial LinkedIn
            $redSocialIn->save();
        }

          // Se crea un instancia del modelo RedSocial Youtube
          $redSocialYt= new RedSocial();
          // Se verifica si el Link de Twitter tiene datos asignan valores a la redSocial Youtube
          if(!empty($request->youtubeEmpresa) || !ctype_space($request->youtubeEmpresa)){
            $redSocialYt->TipoRedSocialID =4;
            $redSocialYt->PymeID = $pyme->id;
            $redSocialYt->Link = $request->youtubeEmpresa;
            // Se guardan los nuevos datos de la redSocial Youtube
            $redSocialYt->save();
          }
          // Se crea un instancia del modelo RedSocial Website
          $redSocialWs= new RedSocial();
          // Se verifica si el Link de Twitter tiene datos asignan valores a la redSocial webEmpresa
          if(!empty($request->webEmpresa) || !ctype_space($request->webEmpresa)){
            $redSocialWs->TipoRedSocialID =5;
            $redSocialWs->PymeID = $pyme->id;
            $redSocialWs->Link = $request->webEmpresa;
            // Se guardan los nuevos datos de la redSocial webEmpresa
            $redSocialWs->save();
          }
          return redirect('/getLoginUsuario');
        } else {
          flash('La combinación de esta Pyme, Usuario, País y Contraseña ya estan registrados', 'danger'); // Si no se guarda despliega esta alerta
          return back();
        }
    }
}

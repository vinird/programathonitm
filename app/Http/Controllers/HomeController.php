<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.2/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\GetDatosPyme;
use App\Respuesta;
use App\Pyme;
use App\Usuario;

use Laracasts\Utilities\JavaScript\JavaScriptFacade as JavaScript;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Config;
/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class HomeController extends Controller
{

    public function getRespuestasPyme($id){
        //Mandar todas las respuestas para la ID del Pyme
        return Respuesta::where( 'PymeID', '=', $id )->get();
    }

    public function getPymes($id){
        return Pyme::where( 'UsuarioID', '=', $id )->get();
    }

    public function getUsuario($id){
        return Usuario::find($id);
    }

    public function getPyme($id){
        return Pyme::find( $id );
    }
    public function esPymeDeUsuario($id, $userId){
        $pyme = Pyme::find( $id );
        if( !$pyme ){
            //No existe la Pime retornar false
            return false;
        }
        //La Pyme Existe verificar si es del Usuario
        if( $pyme->UsuarioID == $userId ){
            //La Pyme es del Usuario repornar TRUE
            return true;
        }else{
            //La Pyme No es del Usuario
            return false;
        }
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('CheckUsuario');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function index( $id = null )
    {
        $userId = session()->get('idUsuario');

        if(!$id || !$this->esPymeDeUsuario( $id, $userId ) ){
            $id = session()->get('idPymeActual');
        }

        //Cargar ID pyme, Nombres
        $respuestas = $this->getRespuestasPyme( $id );
        $pymes = $this->getPymes( $userId );
        $pymeActual = $this->getPyme( $id );
        $usuarioActual = $this->getUsuario( $userId );


        $enlaceEncuesta = 'https://www.facebook.com/sharer/sharer.php?u='.Config::get('app.url')."/encuesta/".$id;

        JavaScript::put([
            'respuestas' => $respuestas,
            'idPyme'     => $pymeActual->Id,
        ]);
        return View::make('home')->with(['respuestas' => $respuestas, 'pymes' => $pymes, 'pyme' => $pymeActual, 'usuario' => $usuarioActual,
        'enlaceEncuesta' => $enlaceEncuesta]);
    }
}
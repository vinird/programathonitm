<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Respuesta;
use App\Pyme;
class GetDatosPyme
{
    public function getRespuestasPyme($id){
        //Mandar todas las respuestas para la ID del Pyme
        return Respuesta::where( 'PymeID', '=', $id )->get();
    }

    public function getPymes($id){
        return Pyme::where( 'UsuarioID', '=', $id )->get();
    }

    public function getPyme($id){
        return Pyme::find( $id );
    }
}

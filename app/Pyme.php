<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pyme extends Model
{
    protected $table = 'pyme';

    public $timestamps = false;
    protected $fillable = [
       'Id', 'NombreComercio', 'EstadoID', 'SectorID', 'AnnoInicioOperaciones', 'NumeroTelefono', 'Direccion', 'EsActiva', 'EsNegocioFamiliar', 'Logo', 'ExtensionLogo', 'FechaCreacion', 'FechaUltimaActualizacion', 'EsFacebookAppInstalado', 'UsuarioID', 'GeneroPropietarioID', 'CedJuridica',
    ];
}

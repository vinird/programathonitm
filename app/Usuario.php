<?php

namespace App;

// use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Usuario extends Authenticatable
{
    protected $table = 'usuario';

    public $timestamps = false;
    protected $fillable = [
        'Id', 'Usuario', 'NombreCompleto', 'EmailContacto', 'Clave'
    ];
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TipoRedSocial extends Model
{
    protected $table = 'tiporedsocial';
    public $timestamps = false;
    protected $fillable = [
        'Id', 'Nombre', 
    ];
}
